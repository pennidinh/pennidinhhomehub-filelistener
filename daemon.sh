#!/bin/bash

inotifywait -mr --timefmt '%d/%m/%y %H:%M' --format '%T %e %w %f' -e delete -e close_write -e moved_to $(echo $WATCH_DIRECTORY) | while read date time event dir file; do
  export FILECHANGE="${dir}${file}"
  echo $FILECHANGE

  if [ "$event" == 'DELETE' ]
  then
    echo 'Delete event'

    response=$(curl -X POST -G --data-urlencode "thumbnailFilePath=$(echo $FILECHANGE)" http://$(echo $NODEJS_HOST)/fileDeletedEvent.js)
    echo "Response when putting deleted file's filepath to server: $(echo $response)"        
  else
    echo 'Write or moved-to event'

    response=$(curl -X POST -G --data-urlencode "thumbnailFilePath=$(echo $FILECHANGE)" http://$(echo $NODEJS_HOST)/pushVideoThumbnail.js)
    echo "Response when putting new file's filepath to server: $(echo $response)"        
  fi
done
